package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Credentials;
import pages.LoginPage;



public class TestLoginInAccount extends SetUpWebDriverManager implements Credentials{

    @Test
    public void login(){
        new LoginPage(driver)
                .getUrl()
                .typeEmail(EMAIL_TEST_USER)
                .typePassword(PASSWORD_TEST_USER)
                .clickButtonLoginandCheckLogo();
    }
    @Test
    public void loginAndCheckRememberButton(){
        new LoginPage(driver)
                .getUrl()
                .rememberButton()
                .loginWithLoginAndPasswordAfterLogout(EMAIL_TEST_USER, PASSWORD_TEST_USER)
                .clickButtonLogin();
    }
    @Test()
    public void signWithEmptyFields(){
        new LoginPage(driver)
                .getUrl()
                .clearFieldsEmailAndPass()
                .clickButtonLogin()
                .checkErrors();
    }
    @Test
    public void signUpWithWrongLoginAndPassword(){
        LoginPage testruner = new LoginPage(driver);
        testruner.getUrl()
                 .typeEmail("asda@tests.com")
                 .typePassword("123sfsad")
                 .clickButtonLogin();
        Assert.assertEquals(testruner.getErrorLoginAndPassword(), "Incorrect email or password");
    }

    @Test
    public void signUpWithCheckErrorsEmptyFields(){
        new LoginPage(driver)
                .getUrl()
                .clickButtonLogin()
                .signUpWithCheckErrorsEmptyFields(EMAIL_TEST_USER, PASSWORD_TEST_USER);
    }


}
