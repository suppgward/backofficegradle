package tests;


import org.testng.annotations.Test;
import pages.MainPage;



public class TestMainPage extends SetUpWebDriverManager {


    @Test
    public void openAndCloseFiltersInCasinoPerformace() {
         new MainPage(driver)
                 .autoLoginInTestAccount()
                 .openCasinoPerformance()
                 .opendAndCloseFiltersMenu();

    }
    @Test
    public void verifyTextHeadersCasinoPerformance() {
        new MainPage(driver)
                .autoLoginInTestAccount()
                .openCasinoPerformance()
                .checkColumnTooltipCasinoPerformance();
    }
    @Test
    public void verifyTextHeadersPlayersPerformance() {
        new MainPage(driver)
                .autoLoginInTestAccount()
                .openPlayersPerformance()
                .checkColumnTooltipPlayersPerformance();
    }
    @Test
    public void verifyTextHeadersGamesPerformance() {
        new MainPage(driver)
                .autoLoginInTestAccount()
                .openGamesPerformance()
                .checkColumnTooltipGamesPerformance();
    }
    @Test
    public void openCalendarChooseTodayAndVerify() {
        new MainPage(driver)
                .autoLoginInTestAccount()
                .openGamesPerformance()
                .openCalendar()
                .chooseTodayAndVerify();
    }
    @Test
    public void openCalendarChooseYesterdayAndVerify()  {
        new MainPage(driver)
                .autoLoginInTestAccount()
                .openGamesPerformance()
                .openCalendar()
                .chooseYesterdayAndVerify();
    }
    @Test
    public void openCalendarChooseWeekToDateAndVerify() {
        new MainPage(driver)
                .autoLoginInTestAccount()
                .openGamesPerformance()
                .openCalendar()
                .chooseWeekToDateAndVerify();
    }
    @Test
    public void openCalendarChooseMonthToDateAndVerify() {
        new MainPage(driver)
                .autoLoginInTestAccount()
                .openGamesPerformance()
                .openCalendar()
                .chooseMonthToDateAndVerify();
    }
    @Test
    public void openCalendarChooseLast7DaysAndVerify() {
        new MainPage(driver)
                .autoLoginInTestAccount()
                .openGamesPerformance()
                .openCalendar()
                .chooseLast7DaysAndVerify();
    }
    @Test
    public void openCalendarChooseLast30DaysAndVerify() {
        new MainPage(driver)
                .autoLoginInTestAccount()
                .openGamesPerformance()
                .openCalendar()
                .chooseLast30DaysAndVerify();
    }
    @Test
    public void openCalendarChooseLast180DaysAndVerify() {
        new MainPage(driver)
                .autoLoginInTestAccount()
                .openGamesPerformance()
                .openCalendar()
                .chooseLast180DaysAndVerify();
    }
    @Test
    public void openCalendarChooseAllPeriodsAndVerify() {
      new MainPage(driver)
              .autoLoginInTestAccount()
              .openGamesPerformance()
              .chooseAllPeriods();
    }

}
