package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Filters extends SetUpPageObject{

    @FindBy(id = "filter-button")
    private WebElement filtersButton;
    @FindBy(xpath = "//div[@class=\"popover-footer\"]/button[1]")
    private WebElement resetButton;
    @FindBy(xpath = "//div[@class=\"popover-footer\"]/button[2]")
    private WebElement cancelButton;
    @FindBy(xpath = "//div[@class=\"popover-footer\"]/button[3]")
    private WebElement applyButton;


    public Filters(WebDriver driver) {
        super(driver);
    }

    public Filters openAndCloseFilters(){
        filtersButton.click();
        cancelButton.click();
        return this;
    }
}
