package pages;

import java.util.Arrays;
import java.util.List;

public interface Credentials {
     String URL = "http://localhost:3000/";
     String EMAIL_TEST_USER = "test@rubyplay.com";
     String PASSWORD_TEST_USER = "changeMe";
     String PATTERN_FOR_CALENDAR = "dd/MM/yyyy";

    List<String> TITLE_COLUMN_CASINO_PERFORMANCE = Arrays.asList(
            "From date",
            "To date",
            "Total spins",
            "Player currency",
            "Total bet",
            "Payout",
            "Net win",
            "RTP",
            "Unique players",
            "New players");

    List<String> TOOLTIP_CASINO_PERFORMANCE = Arrays.asList(
            "Starting date of the period",
            "Ending date of the period",
            "Number of rounds (spins) played within the given period",
            "Currency of the players",
            "Total of bets placed within the given period",
            "Total of wins placed within the given period",
            "Casino Win (Total bets - Total wins or payouts)",
            "Return to player - Payout percentage of the game(s) for the given time period",
            "Total count of unique players who played at least one round - does not matter if using real, bonus money or free spins - in given period",
            "Players that have not played before within the given period");

    List<String> TITLE_COLUMN_PLAYERS_PERFORMANCE = Arrays.asList(
            "Player ID",
            "From date",
            "To date",
            "Player login",
            "Total spins",
            "Player currency",
            "Total bet",
            "Total payout",
            "Net win",
            "RTP");

    List<String> TOOLTIP_PLAYERS_PERFORMANCE = Arrays.asList(
            "Player's unique identifier in BackOffice",
            "Starting date of the period",
            "Ending date of the period",
            "Player's unique username",
            "Number of rounds (spins) played within the given period",
            "Currency of the player",
            "Sum of all bets for the player in the given period",
            "Sum of all game payouts in the given period for the player",
            "Casino Win (Total bets - Total wins or payouts)",
            "Return to player - Payout percentage of the game(s) for the given time period");

    List<String> TITLE_COLUMN_GAMES_PERFORMANCE = Arrays.asList(
            "Game name",
            "From date",
            "To date",
            "Unique players",
            "Total spins",
            "Player currency",
            "Total bet",
            "Total payout",
            "Net win",
            "RTP");

    List<String> TOOLTIP_GAMES_PERFROMANCE = Arrays.asList(
            "Commercial name of the game",
            "Starting date of the period",
            "Ending date of the period",
            "Number of unique players within the given period",
            "Number of rounds (spins) played within the given period",
            "Currency of the players",
            "Sum of all bets for the game in the given period",
            "Sum of all game payouts in the given period",
            "Casino Win (Total bets - Total wins or payouts)",
            "Return to player - Payout percentage of the game(s) for the given time period");
}
