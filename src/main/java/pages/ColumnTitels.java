package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;


import java.util.List;

public class ColumnTitels extends MainPage implements Credentials {
    Actions actions = new Actions(driver);

    public ColumnTitels(WebDriver driver) {
        super(driver);
    }

    public ColumnTitels checkHeadersTitles(List<String> tooltip, List<String>titleColumn){

        for (int i = 0, b = 1, c = 2; i < tooltip.size(); i++, b++, c++){

            String headerElement = String.format("column_title_%s", b);
            String textLocator = String.format("//body/div[%s]//div[@class=\"ant-tooltip-inner\"]", c);
            WebElement title = driver.findElement(By.id(headerElement));
            String titleText = title.getText();
            Assert.assertEquals(titleColumn.get(i), titleText);
            actions.moveToElement(title).perform();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textLocator)));
            String textForAssert = driver.findElement(By.xpath(textLocator)).getText();
            Assert.assertEquals(tooltip.get(i), textForAssert);
        }
        return this;
    }
    public ColumnTitels realiseCheckTooltipCasinoPerformance(){
        checkHeadersTitles(TOOLTIP_CASINO_PERFORMANCE, TITLE_COLUMN_CASINO_PERFORMANCE);
        return this;
    }
    public ColumnTitels realiseCheckTooltipPlayersPerformance(){
        checkHeadersTitles(TOOLTIP_PLAYERS_PERFORMANCE, TITLE_COLUMN_PLAYERS_PERFORMANCE);
        return this;
    }
    public ColumnTitels realiseCheckTooltipGamesPerformance(){
        checkHeadersTitles(TOOLTIP_GAMES_PERFROMANCE, TITLE_COLUMN_GAMES_PERFORMANCE);
        return this;
    }

}
