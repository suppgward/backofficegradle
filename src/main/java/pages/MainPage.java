package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;

public class MainPage extends SetUpPageObject implements Credentials {


    @FindBy (id = "button_profile")
    private WebElement profileButton;
    //    @FindBy(xpath = "//div[@class=\"logout\"]/p")
//    private WebElement logoutButton;
    @FindBy(id = "button_reports")
    private WebElement reportsButton;
    @FindBy(id = "button_settings")
    private WebElement buttonSettings;
    @FindBy(id = "button_logout")
    private WebElement logoutButton;
    @FindBy(id = "casino_performance_plate")
    private WebElement casinoPerformanceOnMidle;
    @FindBy(id = "players_performance_plate")
    private WebElement playersPerformanceOnMidle;
    @FindBy(id = "games_performance_plate")
    private WebElement gamesPerformanceOnMidle;
    @FindBy(xpath = ("//input[@placeholder=\"Start date\"]"))
    private WebElement calendar;


    private By logoRubyPlay = By.className("main-menu__logo");




    public MainPage(WebDriver driver) {
        super(driver);
    }
    public MainPage autoLoginInTestAccount(){
        new LoginPage(driver)
                .getUrl()
                .clearFieldsEmailAndPass()
                .typeEmail(EMAIL_TEST_USER)
                .typePassword(PASSWORD_TEST_USER)
                .clickButtonLoginandCheckLogo();
        return this;
    }
    public MainPage openCasinoPerformance(){
        casinoPerformanceOnMidle.click();
        return this;
    }
    public MainPage openPlayersPerformance(){
        playersPerformanceOnMidle.click();
        return this;
    }
    public MainPage openGamesPerformance(){
        gamesPerformanceOnMidle.click();
        return this;
    }
    public MainPage opendAndCloseFiltersMenu(){
        new Filters(driver).openAndCloseFilters();
        checkLogo();
        return this;
    }



//    public void chooseMainElement(String name){
//        String mainPageElements = String.format("//p[contains(text(), \"%s\")]" , name);
//        driver.findElement(By.xpath(mainPageElements)).click();
//    }

    public LoginPage logout(){
        profileButton.click();
        wait.until(ExpectedConditions.visibilityOf(logoutButton));
        logoutButton.click();
        return new LoginPage(driver);
    }
    public MainPage backInReports(){
        reportsButton.click();
        return this;
    }
    public MainPage checkLogo(){
       wait.until(ExpectedConditions.visibilityOfElementLocated(logoRubyPlay));
       return this;
    }
    public MainPage checkColumnTooltipCasinoPerformance(){
        new ColumnTitels(driver).realiseCheckTooltipCasinoPerformance();
        return this;
    }    public MainPage checkColumnTooltipPlayersPerformance(){
        new ColumnTitels(driver).realiseCheckTooltipPlayersPerformance();
        return this;
    }    public MainPage checkColumnTooltipGamesPerformance(){
        new ColumnTitels(driver).realiseCheckTooltipGamesPerformance();
        return this;
    }
    public MainPage openCalendar(){
        calendar.click();
        return this;
    }

    public MainPage chooseTodayAndVerify(){
        new CalendarPage(driver).chooseTodayAndVerify();
        return this;
    }
    public MainPage chooseYesterdayAndVerify()  {
        new CalendarPage(driver).chooseYesterdayAndVerify();
        return this;
    }
    public MainPage chooseWeekToDateAndVerify(){
        new CalendarPage(driver).chooseWeekToDateAndVerify();
        return this;
    }
    public MainPage chooseMonthToDateAndVerify(){
        new CalendarPage(driver).chooseMonthToDateAndVerify();
        return this;
    }
    public MainPage chooseLast7DaysAndVerify(){
        new CalendarPage(driver).chooseLast7DaysAndVerify();
        return this;
    }
    public MainPage chooseLast30DaysAndVerify(){
        new CalendarPage(driver).chooseLast30DaysAndVerify();
        return this;
    }
    public MainPage chooseLast180DaysAndVerify(){
        new CalendarPage(driver).chooseLast180DaysAndVerify();
        return this;
    }
    public MainPage chooseAllPeriods(){
                 openCalendar()
                .chooseTodayAndVerify()
                .openCalendar()
                .chooseYesterdayAndVerify()
                .openCalendar()
                .chooseWeekToDateAndVerify()
                .openCalendar()
                .chooseMonthToDateAndVerify()
                .openCalendar()
                .chooseLast7DaysAndVerify()
                .openCalendar()
                .chooseLast30DaysAndVerify()
                .openCalendar()
                .chooseLast180DaysAndVerify();
        return this;
    }

}
