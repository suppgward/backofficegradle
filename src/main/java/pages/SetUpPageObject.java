package pages;

import org.apache.http.auth.Credentials;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.security.Principal;

public class SetUpPageObject  {
    public   WebDriver driver;
    public WebDriverWait wait;



    //Constructor
    public SetUpPageObject(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
        PageFactory.initElements(driver, this);


    }
}

