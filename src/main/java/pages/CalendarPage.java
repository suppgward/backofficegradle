package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;

public class CalendarPage extends MainPage implements Credentials{

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Credentials.PATTERN_FOR_CALENDAR);
    Calendar calendar = Calendar.getInstance();

    @FindBy(xpath = "//div[@class=\"ant-calendar-footer-extra ant-calendar-range-quick-selector\"]/span[2]")
    private WebElement todayElement;
    @FindBy(xpath = "//div[@class=\"ant-calendar-footer-extra ant-calendar-range-quick-selector\"]/span[3]")
    private WebElement yesterdayElement;
    @FindBy(xpath = "//div[@class=\"ant-calendar-footer-extra ant-calendar-range-quick-selector\"]/span[4]")
    private WebElement weekToDateElement;
    @FindBy(xpath = "//div[@class=\"ant-calendar-footer-extra ant-calendar-range-quick-selector\"]/span[5]")
    private WebElement monthToDateElement;
    @FindBy(xpath = "//div[@class=\"ant-calendar-footer-extra ant-calendar-range-quick-selector\"]/span[6]")
    private WebElement last7DaysToDateElement;
    @FindBy(xpath = "//div[@class=\"ant-calendar-footer-extra ant-calendar-range-quick-selector\"]/span[7]")
    private WebElement last30DaysToDateElement;
    @FindBy(xpath = "//div[@class=\"ant-calendar-footer-extra ant-calendar-range-quick-selector\"]/span[8]")
    private WebElement last180DaysToDateElement;



    public CalendarPage(WebDriver driver) {
        super(driver);
    }
    public String  getValueStartDate(){
        String toDayStartDateValue = driver.findElement(By.xpath("//input[@placeholder=\"Start date\"]")).getAttribute("value");
        return toDayStartDateValue;
    }
    public String  getValueEndDate(){
        String toDayEndDateValue = driver.findElement(By.xpath("//input[@placeholder=\"End date\"]")).getAttribute("value");
        return toDayEndDateValue;
    }
    public CalendarPage chooseTodayAndVerify(){
        wait.until(ExpectedConditions.visibilityOf(todayElement));
        String todayElemntText = todayElement.getText();
        Assert.assertEquals(todayElemntText, "TODAY");
        todayElement.click();
        String date = simpleDateFormat.format(new Date());
        Assert.assertEquals(date, getValueStartDate());
        Assert.assertEquals(date, getValueEndDate());
        return this;
    }
    public CalendarPage chooseYesterdayAndVerify()  {
        wait.until(ExpectedConditions.visibilityOf(yesterdayElement));
        String todayElemntText = yesterdayElement.getText();
        Assert.assertEquals(todayElemntText, "YESTERDAY");
        yesterdayElement.click();
        calendar.add(Calendar.DATE, -1);
        String calend = simpleDateFormat.format(calendar.getTime());
        Assert.assertEquals(calend, getValueStartDate());
        Assert.assertEquals(calend, getValueEndDate());

        return this;
    }
    public CalendarPage chooseWeekToDateAndVerify(){
        wait.until(ExpectedConditions.visibilityOf(weekToDateElement));
        String todayElemntText = weekToDateElement.getText();
        Assert.assertEquals(todayElemntText, "WEEK TO DATE");
        weekToDateElement.click();
        String date = simpleDateFormat.format(new Date());
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        calendar.add(Calendar.DATE, -dayOfWeek +1);
        String calend = simpleDateFormat.format(calendar.getTime());
        Assert.assertEquals(calend, getValueStartDate());
        Assert.assertEquals(date, getValueEndDate());
        return this;
    }
    public CalendarPage chooseMonthToDateAndVerify(){
        wait.until(ExpectedConditions.visibilityOf(monthToDateElement));
        String todayElemntText = monthToDateElement.getText();
        Assert.assertEquals(todayElemntText, "MONTH TO DATE");
        monthToDateElement.click();
        String date = simpleDateFormat.format(new Date());
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.add(Calendar.DATE, -dayOfMonth+1);
        String calend = simpleDateFormat.format(calendar.getTime());
        Assert.assertEquals(calend, getValueStartDate());
        Assert.assertEquals(date, getValueEndDate());
        return this;
    }
    public CalendarPage chooseLast7DaysAndVerify(){
        wait.until(ExpectedConditions.visibilityOf(last7DaysToDateElement));
        String todayElemntText = last7DaysToDateElement.getText();
        Assert.assertEquals(todayElemntText, "LAST 7 DAYS");
        last7DaysToDateElement.click();
        calendar.add(Calendar.DATE, -1);
        String calend = simpleDateFormat.format(calendar.getTime());
        Assert.assertEquals(calend, getValueEndDate());
        calendar.add(Calendar.DATE, -6);
        String calend1 = simpleDateFormat.format(calendar.getTime());
        Assert.assertEquals(calend1, getValueStartDate());

        return this;
    }
    public CalendarPage chooseLast30DaysAndVerify(){
        wait.until(ExpectedConditions.visibilityOf(last30DaysToDateElement));
        String todayElemntText = last30DaysToDateElement.getText();
        Assert.assertEquals(todayElemntText, "LAST 30 DAYS");
        last30DaysToDateElement.click();
        calendar.add(Calendar.DATE, -1);
        String calend = simpleDateFormat.format(calendar.getTime());
        System.out.println(calend);
        Assert.assertEquals(calend, getValueEndDate());
        calendar.add(Calendar.DATE, -29);
        String calend1 = simpleDateFormat.format(calendar.getTime());
        System.out.println(calend1);
        Assert.assertEquals(calend1, getValueStartDate());
        return this;
    }
    public CalendarPage chooseLast180DaysAndVerify(){
        wait.until(ExpectedConditions.visibilityOf(last180DaysToDateElement));
        String todayElemntText = last180DaysToDateElement.getText();
        Assert.assertEquals(todayElemntText, "LAST 180 DAYS");
        last180DaysToDateElement.click();
        calendar.add(Calendar.DATE, -1);
        String calend = simpleDateFormat.format(calendar.getTime());
        Assert.assertEquals(calend, getValueEndDate());
        calendar.add(Calendar.DATE, -179);
        String calend1 = simpleDateFormat.format(calendar.getTime());
        Assert.assertEquals(calend1, getValueStartDate());
        return this;
    }
}
