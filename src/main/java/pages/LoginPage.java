package pages;




import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import utils.JsonParser;

public class LoginPage extends SetUpPageObject implements Credentials {
    private Actions actions = new Actions(driver);

    private By logoRubyPlay = By.className("copyright__text");
    private By errorMsgLocator = By.id("login_error_message_incorrect");


    @FindBy(id = "login_error_message_empty_email")
    private WebElement emptyLogin;
    @FindBy(id = "login_error_message_empty_password")
    private  WebElement emptyPassword;


    // email for log in
    @FindBy(xpath = "//input[@type=\"user\"]")
    private WebElement email;
    // password for log in
    @FindBy(xpath = "//input[@type=\"password\"]")
    private WebElement password;
    //button log in
    @FindBy(id = "button_login")
    private WebElement buttonLogIn;
    // check box "Remember me"
    @FindBy(id = "checkbox_remember_me")
    private WebElement rememberMeButton;
    // Forgot PassWord? button
    @FindBy(className = "login-form__forgot-link")
    private WebElement forgotPassword;
    //
    @FindBy(id = "login_error_message_incorrect")
    private WebElement wrongLogginOrPassword;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage getUrl(){
        driver.get(URL);
        wait.until(ExpectedConditions.visibilityOfElementLocated(logoRubyPlay));
        return this;
    }
    public LoginPage typeEmail(String mail){
        email.clear();
        email.sendKeys(mail);
        return this;
    }
    public LoginPage typePassword(String pass){
        password.clear();
        password.sendKeys(pass);
        return this;
    }
    public LoginPage clickButtonLogin(){
        buttonLogIn.click();
         return this;
    }
    public MainPage clickButtonLoginandCheckLogo(){
        buttonLogIn.click();
        return new MainPage(driver)
                   .checkLogo();
    }
    public LoginPage rememberButton(){
        actions.moveToElement(rememberMeButton).click().perform();
        return this;
    }
    public void openForgotPassword(){
        forgotPassword.click();
    }

    public LoginPage loginWithLoginAndPasswordAfterLogout(String email, String password){
        typeEmail(email);
        typePassword(password);
        clickButtonLoginandCheckLogo();
        new MainPage(driver).logout();
        return this;
    }


    public MainPage signUpWithCheckErrorsEmptyFields(String login, String pass) {
        WebElement checkLogin = driver.findElement(By.id("login_error_message_empty_email"));
        WebElement checkPassword = driver.findElement(By.id("login_error_message_empty_password"));

        while(true){

            if(emptyLogin.equals(checkLogin)) {
                email.sendKeys(login);
                clickButtonLogin();

            }
            if (emptyPassword.equals(checkPassword)) {
                password.sendKeys(pass);
                clickButtonLogin();

            }
            break;

        }
        return new MainPage(driver)
                   .checkLogo();
    }

    public LoginPage clearFieldsEmailAndPass(){
        email.clear();
        password.clear();
        return this;
    }


    //Errors
    public void checkErrorRequiredFields(){
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(errorMsgLocator));
    }
    public String getErrorEmail(){
        WebElement errorEmail = driver.findElement(By.id("login_error_message_empty_email"));
        return errorEmail.getText();
    }
    public String getErrorPassword(){
        WebElement errorPassword = driver.findElement(By.id("login_error_message_empty_password"));
        return errorPassword.getText();
    }
    public String getErrorLoginAndPassword(){
        return wrongLogginOrPassword.getText();
    }
    public LoginPage checkErrors(){
       String email = JsonParser.parseLang("login_email_empty", "en");
       String password = JsonParser.parseLang("login_password_empty", "en");
        Assert.assertEquals(getErrorEmail(), email);
        Assert.assertEquals(getErrorPassword(), password);
        return this;
    }


}
