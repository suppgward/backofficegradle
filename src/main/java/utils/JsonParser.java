package utils;





import java.io.FileReader;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;


public class JsonParser {
    private  final static String enFilePath = "en.json";
    private  final static String ruFilePath = "ru.json";

    public static String parseLang(String JSONname, String lang) {
        String firstName = null;
        JSONParser jsonParser = new JSONParser();
        if(lang.equals("en")) {
            try (FileReader reader = new FileReader(ClassLoader.getSystemResource(enFilePath).getFile())) {
                JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
                firstName = (String) jsonObject.get(JSONname);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        else if(lang.equals("ru")){
            try (FileReader reader = new FileReader(ClassLoader.getSystemResource(ruFilePath).getFile())) {
                JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
                firstName = (String) jsonObject.get(JSONname);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

    return firstName;
    }
}
